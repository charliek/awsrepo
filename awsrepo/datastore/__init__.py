from awsrepo.datastore.schema import Schema
from awsrepo.model import Release, Package
from boto.dynamodb.exceptions import DynamoDBKeyNotFoundError


class ReleaseData(Schema):

    def add_release(self, release):
        """
        :param release:
        :type release: Release
        :return:
        :rtype:
        """
        attributes = dict(release.std_attributes.items() +
                          release.extra_attributes.items())
        attributes.pop('Codename', None)
        for k, v in attributes.iteritems():
            if isinstance(v, list):
                attributes[k] = set(v)
        item = self.release_table.new_item(
            hash_key=release.codename,
            attrs=attributes
        )
        item.put()

    def get_release(self, codename):
        """
        :param codename:
        :type codename: str
        :return:
        :rtype: Release
        """
        try:
            result = self.release_table.get_item(hash_key=codename)
            return Release(result)
        except DynamoDBKeyNotFoundError:
            return None

    def list_releases(self):
        """
        :return:
        :rtype: list
        """
        results = self.release_table.scan()
        return [Release(item) for item in results]

    def update_release(self, release):
        self.add_release(release)


class PackageData(Schema):

    def add_package(self, package):
        """
        :param package:
        :type package: Package
        """
        pid = str(package)
        item = self.package_table.new_item(hash_key=pid, attrs=package.attributes)
        item.put()

    def get_package(self, pid):
        try:
            result = self.package_table.get_item(hash_key=pid)
            return Package(result)
        except DynamoDBKeyNotFoundError:
            return None

    def list_packages(self):
        results = self.package_table.scan()
        return [Package(item) for item in results]

    def update_package(self, package):
        pid = str(package)
        item = self.package_table.new_item(hash_key=pid, attrs=package.attributes)
        item.put()

    def remove_package(self, pid):
        result = self.package_table.get_item(hash_key=pid)
        if result:
            result.delete()


class PackageInReleaseData(Schema):

    def add_package_to_release(self, pid, codename, component):
        """
        :param pid: package id
        :type pid: str
        :param codename: release codename
        :type codename: str
        :param component: release component
        :type component: str
        """
        item = self.package_in_release_table.new_item(
            hash_key=pid,
            range_key='{}|{}'.format(codename, component))
        item.put()

    def remove_package_from_release(self, pid, codename, component):
        """
        :param pid: package id
        :type pid: str
        :param codename: release codename
        :type codename: str
        :param component: release component
        :type component: str
        """
        item = self.package_in_release_table.get_item(
            hash_key=pid,
            range_key='{}|{}'.format(codename, component))
        if item:
            item.delete()

    # def get_components_in_release(self, codename):
    #     """
    #     :param codename: release codename
    #     :type codename: str
    #     :return: list of components in the release
    #     :rtype: list
    #     """
    #     import boto.dynamodb.condition
    #     result = self.package_in_release_table.scan(
    #         scan_filter={'Codename': boto.dynamodb.condition.BEGINS_WITH(codename)}
    #     )
    #     return [item['Codename'].split('|')[1] for item in result]

    def get_packages_in_component(self, codename, component):
        import boto.dynamodb.condition
        result = self.package_in_release_table.scan(
            scan_filter={'Codename': boto.dynamodb.condition.EQ('{}|{}'.format(codename, component))}
        )
        ids = list(set(item['ID'] for item in result))
        package_items = self.package_table.batch_get_item(keys=ids)
        return [Package(p) for p in package_items]

    def get_packages_in_release(self, codename):
        """
        :param codename: release codename
        :type codename: str
        :return: dict of components and Packages
        :rtype: dict
        """
        import boto.dynamodb.condition
        result = self.package_in_release_table.scan(
            scan_filter={'Codename': boto.dynamodb.condition.BEGINS_WITH(codename)}
        )

        # load packages
        items = [(
                     item['Codename'].split('|')[1],
                     item['ID']
                 ) for item in result]

        package_items = self.package_table.batch_get_item(keys=list(set([k[1] for k in items])))

        # create map of packages
        packages = {p['ID']: Package(p) for p in package_items}

        # create map component/package lists
        release = {}
        for item in items:
            component = item[0]
            pid = item[1]
            package = packages[pid]
            if not component in release:
                release[component] = []

            release[component].append(packages[pid])

        return release

class DynamoDBDataStore(ReleaseData, PackageData, PackageInReleaseData):

    def __init__(self, db, prefix):
        """
        :param db: dynamodb boto connection
        :type db: :class:`boto.dynamodb.layer2.Layer2`
        :param prefix: dynamodb table prefix
        :type prefix: str
        """
        self.initialize_schema(db, prefix)