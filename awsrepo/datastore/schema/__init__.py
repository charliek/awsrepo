from boto.exception import DynamoDBResponseError
from boto.dynamodb.table import Table
from boto.dynamodb.layer2 import Layer2
from ConfigParser import ConfigParser


class Schema(object):

    @property
    def package_table(self):
        return self.__package_table

    @property
    def release_table(self):
        return self.__release_table

    @property
    def package_in_release_table(self):
        return self.__package_in_release_table

    """ DynamoDB schema mixin
    """
    def initialize_schema(self, connection, prefix):
        """
        :param prefix:
        :type prefix: str
        :param connection:
        :type connection: Layer2
        :return:
        :rtype:
        """

        def get_or_create(name, schema):
            """
            :param name:
            :type name: str
            :param schema:
            :type schema: Schema
            :return:
            :rtype: Table
            """
            try:
                return connection.get_table(name)
            except DynamoDBResponseError:
                return connection.create_table(name, schema, 1, 1)

        self.__release_table = get_or_create(
            name=prefix + 'release',
            schema=connection.create_schema(
                hash_key_name='Codename',
                hash_key_proto_value='S')
        )

        self.__package_table = get_or_create(
            name=prefix + 'package',
            schema=connection.create_schema(
                hash_key_name='ID',
                hash_key_proto_value='S'
            )
        )

        self.__package_in_release_table = get_or_create(
            name=prefix + 'package_in_release',
            schema=connection.create_schema(
                hash_key_name='ID',
                hash_key_proto_value='S',
                range_key_name='Codename',
                range_key_proto_value='S'
            )
        )