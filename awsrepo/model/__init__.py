

class Release(object):

    __std_attributes = ['Origin', 'Label', 'Description', 'Version',
                        'Suite', 'Codename', 'Components', 'Architectures']
    __attributes = None

    def __init__(self, attributes):
        self.__attributes = attributes

    def __getitem__(self, key):
        return self.__attributes[key]

    def __iter__(self):
        for x in self.__attributes:
            yield self.__attributes[x]

    @property
    def std_attributes(self):
        return {k: v for (k, v) in self.__attributes.items() if k in Release.__std_attributes}

    @property
    def extra_attributes(self):
        return {k: v for (k, v) in self.__attributes.items() if k not in Release.__std_attributes}

    @property
    def origin(self):
        return self.__attributes.get('Origin')

    @property
    def label(self):
        return self.__attributes.get('Label')

    @property
    def suite(self):
        return self.__attributes.get('Suite')

    @property
    def codename(self):
        return self.__attributes.get('Codename')

    @property
    def description(self):
        return self.__attributes.get('Description')

    @property
    def version(self):
        return self.__attributes.get('Version')

    @property
    def components(self):
        return self.__attributes['Components'] if 'Components' in self.__attributes else set()

    @property
    def architectures(self):
        return self.__attributes['Architectures'] if 'Architectures' in self.__attributes else set()


class Package(object):

    __std_attributes = ['Package', 'Version', 'Maintainer', 'Vendor', 'Homepage', 'Section',
                        'License', 'Architecture', 'Description', 'Installed-Size',
                        'SHA1', 'SHA256', 'MD5sum', 'Size']

    __relations_attributes = ['Depends', 'Recommends', 'Suggests', 'Enhances', 'Pre-Depends',
                              'Breaks', 'Conflicts', 'Provides', 'Replaces']

    __attributes = {}

    __version = None

    def __init__(self, attributes):
        self.__attributes = attributes
        self.__version = Version.from_str(attributes['Version'])

    def __getitem__(self, key):
        return self.__attributes[key]

    def __iter__(self):
        for x in self.__attributes:
            yield self.__attributes[x]

    def __eq__(self, other):
        return (
            other and self.md5 == other.md5
            and self.sha1 == other.sha1 and
            self.sha256 == other.sha256)

    def __hash__(self):
        return hash(self.md5) + hash(self.sha1) + hash(self.sha256)

    def __str__(self):
        version = str(self.version)
        return '{}_{}_{}'.format(self.name, version, self.architecture)

    @property
    def std_attributes(self):
        return {k: v for (k, v) in self.attributes.items() if k in Package.__std_attributes}

    @property
    def extra_attributes(self):
        return {k: v for (k, v) in self.attributes.items()
                if k not in Package.__std_attributes and k not in Package.__relations_attributes}

    @property
    def relations_attributes(self):
        return {k: v for (k, v) in self.attributes.items() if k in Package.__relations_attributes}

    @property
    def name(self):
        return self.__attributes.get('Package')

    @property
    def version(self):
        return self.__version

    @property
    def maintainer(self):
        return self.__attributes.get('Maintainer')

    @property
    def vendor(self):
        return self.__attributes.get('Vendor')

    @property
    def homepage(self):
        return self.__attributes.get('Homepage')

    @property
    def section(self):
        return self.__attributes.get('Section')

    @property
    def license(self):
        return self.__attributes.get('License')

    @property
    def architecture(self):
        return self.__attributes.get('Architecture')

    @property
    def description(self):
        return self.__attributes.get('Description')

    @property
    def installed_size(self):
        return self.__attributes.get('Installed-Size')

    @property
    def attributes(self):
        return self.__attributes.copy()

    # relationships
    # http://www.debian.org/doc/debian-policy/ch-relationships.html
    @property
    def depends(self):
        return self.__attributes.get('Depends')

    @property
    def recommends(self):
        return self.__attributes.get('Recommends')

    @property
    def suggests(self):
        return self.__attributes.get('Suggests')

    @property
    def enhances(self):
        return self.__attributes.get('Enhances')

    @property
    def pre_depends(self):
        return self.__attributes.get('Pre-Depends')

    @property
    def breaks(self):
        return self.__attributes.get('Breaks')

    @property
    def conflicts(self):
        return self.__attributes.get('Conflicts')

    @property
    def provides(self):
        return self.__attributes.get('Provides')

    @property
    def replaces(self):
        return self.__attributes.get('Replaces')

    # hashes
    @property
    def sha1(self):
        return self.__attributes.get('SHA1')

    @property
    def sha256(self):
        return self.__attributes.get('SHA256')

    @property
    def md5(self):
        return self.__attributes.get('MD5sum')

    @property
    def size(self):
        return self.__attributes.get('Size')


class PackageInRelease(object):

    __attributes = None

    def __init__(self, attributes):
        self.__attributes = attributes

    def __getitem__(self, key):
        return self.__attributes[key]

    def __iter__(self):
        for x in self.__attributes:
            yield self.__attributes[x]

    @property
    def attributes(self):
        return self.__attributes.copy()

    @property
    def package_id(self):
        return self.__attributes.get('ID')

    @property
    def codename(self):
        return self.__attributes.get('Codename')

    @property
    def architecture(self):
        return self.__attributes.get('Architecture')

    @property
    def component(self):
        return self.__attributes.get('Component')


class Version(object):

    @classmethod
    def from_str(cls, value):
        import re
        # Parse 'epoch:version-iteration' in the version string
        match = re.match(r'^(?:([0-9]+):)?(.+?)(?:-(.*))?$', value)
        if not match:
            raise AttributeError("unsupported version string '{}'".format(value))
        else:
            return Version(*match.groups())

    def __init__(self, epoch=None, version=None, iteration=None):
        self.__epoch = epoch
        self.__version = version
        self.__iteration = iteration

    def __str__(self):
        return (
            ('{}:'.format(self.epoch) if self.epoch else '') +
            self.version +
            ('-{}'.format(self.iteration) if self.iteration else '')
        )

    def short_str(self):
        return (
            self.version +
            ('-{}'.format(self.iteration) if self.iteration else '')
        )

    @property
    def epoch(self):
        return self.__epoch

    @property
    def version(self):
        return self.__version

    @property
    def iteration(self):
        return self.__iteration