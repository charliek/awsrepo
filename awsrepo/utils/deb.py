import os
import re
import tempfile
import subprocess
import shutil
import hashlib
import logging
from awsrepo.model import Package, Version

log = logging.getLogger(__name__)

def parse_deb(deb):
    """Parse package control file from Debian package
    :param deb: path to Debian package
    :type deb: str
    :rtype: Package
    """
    temp_dir = tempfile.mkdtemp()
    cmd = 'ar p {0} control.tar.gz | tar -zxf - -C {1}'.format(deb, temp_dir)
    subprocess.call(cmd, shell=True)
    with open(os.path.join(temp_dir, 'control'), 'r') as f:
        control = f.read()
    shutil.rmtree(temp_dir)
    control_attributes = __parse(control)
    # control file doesn't contain the following but is needed for metadata
    bts = open(deb).read()
    control_attributes['Size'] = os.path.getsize(deb)
    control_attributes['SHA1'] = hashlib.sha1(bts).hexdigest()
    control_attributes['SHA256'] = hashlib.sha256(bts).hexdigest()
    control_attributes['MD5sum'] = hashlib.md5(bts).hexdigest()
    return Package(control_attributes)


def __parse(control):
    """
    Parse package control file
    """
    def clean(key, value):
        key = key.strip()
        value = value.strip()
        return key, value

    attr = {}
    lines = control.split('\n')
    for line in lines:
        # description bock starts with a space
        if re.match(r'^ .*\n?$', line):
            attr['Description'] += '\n'+line.strip()
        # normal
        elif re.match(r'^\S+: .*\n?$', line):
            key, value = clean(*line.split(': '))
            attr[key] = value
        else:
            log.warning("unexpected control entry '{0}'".format(line.strip()))
    return attr
