import re
from StringIO import StringIO
import hashlib
from jinja2 import Environment, PackageLoader

__env = Environment(loader=PackageLoader('awsrepo', 'templates'))


class Indexer():

    def __init__(self, release, packages):
        self.__components = release.components
        self.__architectures = release.architectures
        self.__release = release
        self.__packages = packages

    def index(self):

        def idx_path(comp, arch, name, gz=False):
            return '{}/binary-{}/{}'.format(comp, arch, name) \
                if not gz else \
                '{}/binary-{}/{}.gz'.format(comp, arch, name)

        collection = collect(
            self.__components, self.__architectures, self.__packages)

        # create index for each component/arch
        package_idxs = [
            idx_packages(c, a, collection[c][a]) for c in collection for a in collection[c]
        ]

        # create architecture release
        arch_idxs = [
            idx_architecture(self.__release, c, a)
            for c in collection for a in collection[c]]

        component_idxs = package_idxs + arch_idxs

        # compute hashes
        hashes = {'MD5Sum': [], 'SHA1Sum': [], 'SHA256Sum': []}
        for idx in component_idxs:
            comp = idx[0]
            arch = idx[1]
            name = idx[2]
            file_raw = idx[3].getvalue()
            size_raw = idx[3].len

            # hash uncompressed
            hashes['MD5Sum'].append(
                (hashlib.md5(file_raw).hexdigest(), str(size_raw), idx_path(comp, arch, name, False)))
            hashes['SHA1Sum'].append(
                (hashlib.sha1(file_raw).hexdigest(), str(size_raw), idx_path(comp, arch, name, False)))
            hashes['SHA256Sum'].append(
                (hashlib.sha256(file_raw).hexdigest(), str(size_raw), idx_path(comp, arch, name, False)))

            # hash compressed if we've got them
            if len(idx) > 4:
                file_gz = idx[4].getvalue()
                size_gz = idx[4].len
                hashes['MD5Sum'].append(
                    (hashlib.md5(file_gz).hexdigest(), str(size_gz), idx_path(comp, arch, name, True)))
                hashes['SHA1Sum'].append(
                    (hashlib.sha1(file_gz).hexdigest(), str(size_gz), idx_path(comp, arch, name, True)))
                hashes['SHA256Sum'].append(
                    (hashlib.sha256(file_gz).hexdigest(), str(size_gz), idx_path(comp, arch, name, True)))

        # create component release
        release_idx = idx_release(self.__release, self.__components, self.__architectures, hashes)

        return Index(release_idx, arch_idxs, package_idxs)


class Index(object):

    def __init__(self, release, arch, packages):
        self.__release = release
        self.__arch = arch
        self.__packages = packages

    @property
    def release(self):
        return self.__release

    @property
    def arch(self):
        return self.__arch

    @property
    def packages(self):
        return self.__packages


def gzip(source):
    import gzip
    sio = StringIO()
    gzf = gzip.GzipFile(fileobj=sio, mode='wb')
    gzf.write(source.getvalue())
    gzf.close()
    sio.seek(0)
    return sio


def collect(components, architectures, packages):

    o = {}
    for c in components:
        for a in architectures:
            if not c in o:
                o[c] = {}
            if not a in o[c]:
                o[c][a] = []

    # collect packages by component/architecture
    for component in components:
        if component in packages:
            component_packages = packages[component]
            for pkg in component_packages:
                # add 'all' architecture to all architectures
                if pkg.architecture == 'all':
                    for arch in architectures:
                        o[component][arch].append(pkg)
                elif pkg.architecture in architectures:
                    o[component][pkg.architecture].append(pkg)

    return o


def idx_release(release, components, architectures, hashes):
    attr = {
        'origin': release.origin,
        'label': release.label,
        'suite': release.suite,
        'codename': release.codename,
        'components': ', '.join(components),
        'architectures': ', '.join(architectures),
        'md5sum': hashes['MD5Sum'],
        'sha1sum': hashes['SHA1Sum'],
        'sha256sum': hashes['SHA256Sum'],
    }
    template = __env.get_template('release.jinja')
    sio = StringIO()
    sio.write(template.render(**attr))
    sio.seek(0)
    return release.codename, sio


def idx_architecture(release, comp, arch):
    attr = {
        'archive': release.codename,
        'component': comp,
        'origin': release.origin,
        'label': release.label,
        'arch': arch
    }
    template = __env.get_template('architecture.jinja')
    sio = StringIO()
    sio.write(template.render(**attr))
    sio.seek(0)
    return comp, arch, 'Release', sio


def idx_packages(comp, arch, packages):
    indexes = [idx_package(p) for p in packages]
    index = '\n\n'.join(indexes)
    sio = StringIO()
    sio.write(index)
    sio.seek(0)
    return comp, arch, 'Packages', sio, gzip(sio)


def idx_package(package):
    std = package.std_attributes
    extra = package.extra_attributes
    relations = package.relations_attributes

    # handle special cases
    if 'Description' in std:
        d = std['Description']
        des_lines = d.split('\n')
        std['Description'] = '\n '.join(des_lines)

    if not 'Filename' in std:
        name = std['Package']
        # don't include the epoch in file name as customary
        version_str = package.version.short_str()
        arch = std['Architecture']
        prefix = name[:4] if re.match(r'^lib', name) else name[:1]
        fullname = '_'.join([name, version_str, arch])
        filename = 'pool/{}/{}/{}.deb'.format(prefix, name, fullname)
        std['Filename'] = filename

    if 'Version' in std:
        std['Version'] = str(std['Version'])

    # make keys jinja friendly
    std = {k.replace('-', '_').lower(): v for (k, v) in std.items()}
    std['extra'] = extra
    std['relations'] = relations
    template = __env.get_template('package.jinja')
    return template.render(**std)