import os
import tempfile
import shutil
from awsrepo.model import Package
from awsrepo.filestore import Writer
from unittest import TestCase

class TestWriter(TestCase):

    def setUp(self):
        self.__tempdir = tempfile.mkdtemp()
        pubring = os.path.join(os.path.dirname(__file__), '../', '..', 'resources', 'pubring.gpg')
        secring = os.path.join(os.path.dirname(__file__), '../', '..', 'resources', 'secring.gpg')
        trustdb = os.path.join(os.path.dirname(__file__), '../', '..', 'resources', 'trustdb.gpg')
        shutil.copyfile(pubring, os.path.join(self.__tempdir, 'pubring.gpg'))
        shutil.copyfile(secring, os.path.join(self.__tempdir, 'secring.gpg'))
        shutil.copyfile(trustdb, os.path.join(self.__tempdir, 'trustdb.gpg'))

    def tearDown(self):
        shutil.rmtree(self.__tempdir)

    def test_sign(self):

        writer = Writer()
        data = writer.sign('test 123', '243659B73288D31D', 'temp123', gpg_binary='/usr/local/bin/gpg', gpg_home=self.__tempdir)

        self.assertTrue(data.len > 0)

    def test_get_pool_path(self):
        package = Package({
            'Package':'foo',
            'Version':'0.1-1',
            'Architecture':'all',
            'Maintainer':'John Doe <jdoe@awesome.org>',
            'Installed-Size':29,
            'Depends':'bar (>= 1.0), libbar',
            'Section':'misc',
            'Priority':'extra',
            'Homepage':'http://www.theuselessweb.com',
            'Description':(
                'just a test\n'
                'another line'
            ),
            'Size':'1698',
            'MD5sum':'abc',
            'SHA1':'zyx',
            'SHA256':'123'
        })
        writer = Writer()

        path = writer.get_pool_path(package)
        self.assertEqual(path, ['f', 'foo', 'foo_0.1-1_all.deb'])

