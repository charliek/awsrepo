import os
from unittest import TestCase
from awsrepo.utils import deb


class TestDebUtils(TestCase):

    def test_parse(self):
        foo = os.path.join(os.path.dirname(__file__), '..', '..', 'resources', 'foo_0.1-1_all.deb')
        package = deb.parse_deb(foo)

        self.assertEqual('foo', package.name)
        self.assertEqual('bar (>= 1.0), libbar', package.depends)
        self.assertEqual('700985d8f1589c1d781015fe752c52bc', package.md5)
        self.assertEqual('7a3e460382407296e244c8470c4840fcf15d8488', package.sha1)
        self.assertEqual('759840a77bb7e5b93536b291524a67f6b486e39c576161873ff787a9f008a3c9', package.sha256)
        self.assertEqual(1698, package.size)
